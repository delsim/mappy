#!/usr/bin/env bash
#
source env/bin/activate

mkdir -p site
mkdir -p site/img
mkdir -p site/css
mkdir -p site/js

cp -R css/* site/css/
cp -R js/* site/js/
cp -R img/* site/img/

python build_it.py
