#!/usr/bin/env bash
#
#virtualenv env -p python3.7
python3 -m venv env python3
source env/bin/activate
pip3 install --upgrade pip3
pip3 install -r scripts/requirements.txt

./scripts/setup.bash


