import mappy

from jinja2 import Environment, PackageLoader, select_autoescape

from PIL import Image, ImageDraw


env = Environment(loader=PackageLoader("mappy"),
                  autoescape=select_autoescape())


templates = ['index.html',
             'map2.html',
             'rect_one.html',
             'rect_two.html',
             'rect_three.html',
             'rect_four.html',
             'rect_six.html',
             'rect_five.html',
             'rect_seven.html',
             'rect_eight.html']


red_fill = (255, 0, 0, 100)
green_fill = (0, 255, 0, 100)
blue_fill = (0, 0, 255, 100)
yellow_outline = (255, 255, 0, 255)
blue_outline = (0, 0, 255, 255)


auch = ('auchwitz_overview', 800, 640, 174, 140, 'jpeg',
        [('map2.html',[(46, 0), (80,0), (83,16), (59,22), (59,16), (51,19), (51,24)], red_fill, yellow_outline),
        ('rect_seven.html',[(51, 16), (59,16), (59,24), (51,24)], blue_fill, yellow_outline),
         ('rect_six.html',[(46, 25), (67,25), (67,44), (46,44)], green_fill, yellow_outline),
         ])


berg = ('sshot1', 477, 562, 124, 147, 'png',
        [('rect_one.html', [(94,56),(100,55),(105,97),(99,98)], red_fill, yellow_outline),
         ('rect_two.html', [(84,56),(94,55),(100,100),(90,101)], red_fill, yellow_outline),
         ('rect_eight.html', [(74,57),(84,56),(89,101),(79,102)], green_fill, yellow_outline),
         ('rect_five.html', [(53,59),(64,58),(69,102),(58,103)], red_fill, yellow_outline),
         ('rect_four.html', [(26, 64),(47,62),(49,81),(28,83)], red_fill, yellow_outline),
         ('rect_three.html', [(52,109),(108,104),(109,127),(53,132)], green_fill, yellow_outline),
         ])


def process_single_item(line_item, fact_x, fact_y):
    target = line_item[0]
    iname = target
    path = line_item[1]
    coords_list_i = []
    for x,y in path:
        coords_list_i.append(int(x * fact_x))
        coords_list_i.append(int(y * fact_y))
    coords_list = [str(x) for x in coords_list_i]
    coords = ",".join(coords_list)
    return (f'  <area shape="poly" coords="{coords}" alt="{iname}" href="{target}"></area>',
            coords_list_i)


def process_item(item, tag):
    img_name = item[0]
    width = item[1]
    height = item[2]
    fact_x = width / item[3]
    fact_y = height / item[4]
    file_type = item[5]
    base_text = f'''<img src="img/{img_name}.png" height="{height}" width="{width}" usemap="#{tag}"></img>
<map name="{tag}">
'''
    src_image_name = f"{img_name}.{file_type}"
    img = Image.open(f"img/{src_image_name}").convert('RGBA')
    overlay = Image.new('RGBA', img.size, (0, 0, 0, 0))
    draw = ImageDraw.Draw(overlay, 'RGBA')

    lines2 = [process_single_item(line_item, fact_x, fact_y) for line_item in item[6]]
    lines = [x for x, _ in lines2]
    svgs = [x for _, x in lines2]

    for line, line_item in zip(svgs, item[6]):
        fill = line_item[2]
        outline = line_item[3]
        draw.polygon(line, fill=fill, outline=outline)
    Image.alpha_composite(img, overlay).save(f'site/img/{img_name}.png')

    return '''
<div class="img_wrapped">
<div class="img_box">
    ''' + base_text + "\n".join(lines) + '''
</map>
</div>
</div>'''


auch_items = process_item(auch, "mapauch")
b2_items = process_item(berg, 'mapberg')


for template_name in templates:
    template = env.get_template(template_name)
    rtemp = template.render(locals())
    with open(f"site/{template_name}", "w") as out_file:
        print(rtemp, file=out_file)
